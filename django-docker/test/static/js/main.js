$( document ).ready(function() {

  const CHART = document.getElementById("lineChart");
  console.log(CHART);
  let lineChart = new Chart(CHART, {
    type : 'line',
    data : {
      labels : [ "January", "February", "March", "April", "May", "June", "July" ],
      datasets : [
        {
          label : "Tomato",
          fill: false,
          backgroundColor: '#2cb5e8',
          borderColor: '#2cb5e8',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#2cb5e8',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#2cb5e8',
          pointHoverBorderColor: '#2cb5e8',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [10,35,21,16,46,65,73],
        },
        {
          label : "Carrots",
          fill: false,
          backgroundColor: '#009494',
          borderColor: '#009494',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#009494',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#009494',
          pointHoverBorderColor: '#009494',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [16,65,31,23,50,85,23],
        }

      ]
    }
  });


const CHART2 = document.getElementById("lineChart2");
  console.log(CHART2);
  let lineChart2 = new Chart(CHART2, {
    type : 'line',
    data : {
      labels : [ "January", "February", "March", "April", "May", "June", "July" ],
      datasets : [
        {
          label : "Tomato",
          fill: false,
          backgroundColor: '#2cb5e8',
          borderColor: '#2cb5e8',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#2cb5e8',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#2cb5e8',
          pointHoverBorderColor: '#2cb5e8',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [10,35,21,16,46,65,73],
        },
        {
          label : "Carrots",
          fill: false,
          backgroundColor: '#009494',
          borderColor: '#009494',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#009494',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#009494',
          pointHoverBorderColor: '#009494',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [16,65,31,23,50,85,23],
        }

      ]
    }
  });

  const CHART3 = document.getElementById("lineChart3");
  console.log(CHART3);
  let lineChart3 = new Chart(CHART3, {
    type : 'line',
    data : {
      labels : [ "January", "February", "March", "April", "May", "June", "July" ],
      datasets : [
        {
          label : "Tomato",
          fill: false,
          backgroundColor: '#2cb5e8',
          borderColor: '#2cb5e8',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#2cb5e8',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#2cb5e8',
          pointHoverBorderColor: '#2cb5e8',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [10,35,21,16,46,65,73],
        },
        {
          label : "Carrots",
          fill: false,
          backgroundColor: '#009494',
          borderColor: '#009494',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#009494',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#009494',
          pointHoverBorderColor: '#009494',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [16,65,31,23,50,85,23],
        }

      ]
    }
  });


  const CHART4 = document.getElementById("lineChart4");
  console.log(CHART4);
  let lineChart4 = new Chart(CHART4, {
    type : 'line',
    data : {
      labels : [ "January", "February", "March", "April", "May", "June", "July" ],
      datasets : [
        {
          label : "Tomato",
          fill: false,
          backgroundColor: '#2cb5e8',
          borderColor: '#2cb5e8',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#2cb5e8',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#2cb5e8',
          pointHoverBorderColor: '#2cb5e8',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [10,35,21,16,46,65,73],
        },
        {
          label : "Carrots",
          fill: false,
          backgroundColor: '#009494',
          borderColor: '#009494',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderWidth: 2,
          borderJoinStyle: 'miter',
          pointBorderColor: '#009494',
          pointStyle: 'circle',
          pointBorderWidth: 5,
          pointHoverRadius: 10,
          pointHoverBackgroundColor: '#009494',
          pointHoverBorderColor: '#009494',
          pointHoverBorderWidth: 1,
          pointRadius: 1,
          pointHitRadius: 20,
          data : [16,65,31,23,50,85,23],
        }

      ]
    }
  });
});   
