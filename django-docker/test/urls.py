from django.conf.urls import url
from . import views

app_name = 'portal'

urlpatterns = [
    url(r'^dashboard$', views.dashboard, name='dashboard'),
    url(r'^farmers$', views.farmers, name='farmers'),
    url(r'^products$', views.products, name='products'),
    url(r'^report$', views.report, name='report'),
    url(r'^investment$', views.investment, name='investment'),
    url(r'^profile$', views.profile, name='profile'),
    url(r'^add/Farmer$', views.addFarmer, name='add-farmer'),
    url(r'^add/Aide$', views.addAide, name='add-aide'),
    url(r'^aide$', views.aide, name='aide'),
    url(r'^add/Investor$', views.addInvestor, name='add-investor'),
    url(r'^farmer/Add/Products$', views.farmerAddProduct, name='farmer-add-product'),
    url(r'^farmer/Dashboard$', views.farmerDashboard, name='farmer-dashboard'),
    url(r'^farmer/Products$', views.farmerProducts, name='farmer-products'),
    url(r'^farmer/Profile$', views.farmerProfile, name='farmer-profile'),
    url(r'^farmer/Report$', views.farmerReport, name='farmer-report'),
    url(r'^$', views.website, name='website'),
    url(r'^investor$', views.investor, name='investor'),
    url(r'^investor/History$', views.investorHistory, name='investor-history'),
    url(r'^investor/Token$', views.investorToken, name='investor-token'),
    url(r'^investor/Profile$', views.investorProfile, name='investor-profile'),
    url(r'^login$', views.login, name='login'),
]
