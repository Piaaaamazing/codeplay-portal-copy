from django.http import HttpResponse
from django.shortcuts import render, redirect


def dashboard(request):
    args = { }
    return render(request, 'dashboard.html', args)


def farmers(request):
    args = { }
    return render(request, 'farmers.html', args)


def products(request):
    args = { }
    return render(request, 'products.html', args)


def report(request):
    args = { }
    return render(request, 'report.html', args)


def investment(request):
    args = { }
    return render(request, 'investment.html', args)


def profile(request):
    args = { }
    return render(request, 'profile.html', args)


def addFarmer(request):
    args = { }
    return render(request, 'add-farmer.html', args)


def addAide(request):
    args = { }
    return render(request, 'add-aide.html', args)


def aide(request):
    args = { }
    return render(request, 'aide.html', args)


def addInvestor(request):
    args = { }
    return render(request, 'add-investor.html', args)


def farmerAddProduct(request):
    args = { }
    return render(request, 'farmer-add-product.html', args)


def farmerDashboard(request):
    args = { }
    return render(request, 'farmer-dashboard.html', args)


def farmerProducts(request):
    args = { }
    return render(request, 'farmer-products.html', args)


def farmerProfile(request):
    args = { }
    return render(request, 'farmer-profile.html', args)


def farmerReport(request):
    args = { }
    return render(request, 'farmer-report.html', args)


def website(request):
    args = { }
    return render(request, 'website.html', args)


def investorHistory(request):
    args = { }
    return render(request, 'investor-history.html', args)


def investor(request):
    args = { }
    return render(request, 'investor.html', args)


def investorToken(request):
    args = { }
    return render(request, 'investor-token.html', args)


def investorProfile(request):
    args = { }
    return render(request, 'investor-profile.html', args)


def login(request):
    args = { }
    return render(request, 'login.html', args)